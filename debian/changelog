django-rq (3.0-1) unstable; urgency=medium

  * [96d1d9d] d/watch: Watch more flexible on available versions
  * [13c19ac] New upstream version 3.0
  * [050af17] d/rules: Drop obsolete setting PYBUILD_SYSTEM=distutils
    We use already dh-sequence-python3 as a build dependency and no distutils
    stuff is used at all.
  * [493bdab] d/rules: Drop --with option from default target
    Also obdolete by using dh-sequence-python3.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 12 Nov 2024 18:16:45 +0100

django-rq (2.10.2-1) unstable; urgency=medium

  * [46ea52f] New upstream version 2.10.2
  * [0367cb3] d/control: Update Standards-Version to 4.7.0
  * [3f68dc6] d/copyright: Update year data

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 21 Apr 2024 19:20:39 +0200

django-rq (2.10.1-1) unstable; urgency=medium

  * [d23df32] New upstream version 2.10.1

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 31 Dec 2023 07:05:57 +0100

django-rq (2.9.0-1) unstable; urgency=medium

  * [4c87dbe] d/gbp.conf: Drop doubled setting for debian-branch
  * [e8ed8cc] d/watch: Set compression type to gz
  * [eb0922a] New upstream version 2.9.0

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 02 Dec 2023 09:09:49 +0100

django-rq (2.8.1-2) unstable; urgency=medium

  * Upload to unstable

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 20 Jun 2023 19:34:00 +0200

django-rq (2.8.1-1) experimental; urgency=medium

  * [ebb8bf3] New upstream version 2.8.1

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 23 May 2023 16:49:35 +0200

django-rq (2.8.0-1) experimental; urgency=medium

  * [4d5f24c] d/gbp.conf: Add files to filter out
  * [082b034] d/README.source: Basic information about source handling
  * [7901560] New upstream version 2.8.0

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 04 May 2023 19:28:26 +0200

django-rq (2.7.0-1) unstable; urgency=medium

  * [eda593f] New upstream version 2.7.0
  * [e64669f] d/control: Update Standards-Version to 4.6.2
    No further changes needed.
  * [0d5fc39] d/copyright: Update year data
  * [b19f216] d/control: Use dh-sequence-python3 instead of dh-python

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 09 Feb 2023 12:18:19 +0100

django-rq (2.6.0-1) unstable; urgency=medium

  * [6023f43] New upstream version 2.6.0
  * [d1b9a97] d/control: Update Standards-Version to 4.6.0
    No further changes needed.
  * [e7900eb] Lintian: Drop not needed override

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 05 Nov 2022 12:57:31 +0100

django-rq (2.5.1-1) unstable; urgency=medium

  * [2f0db05] New upstream version 2.5.1

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 23 Nov 2021 16:47:03 +0100

django-rq (2.5.0-1) unstable; urgency=medium

  * [57ff99e] New upstream version 2.5.0
  * [4cdc86e] d/control: Update Standards-Version to 4.6.0
    No further changes needed.
  * [cb7028b] d/watch: Correct package name in filenamemangle

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 19 Nov 2021 18:06:31 +0100

django-rq (2.4.1-1~exp1) experimental; urgency=medium

  * [0ee0499] New upstream version 2.4.1
  * [3870e1b] First and basic Debianization
    (Closes: #990085)

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 21 Jun 2021 20:32:45 +0200
